<?php
namespace App\Deploy;

require 'vendor/autoload.php';
include("ConnectionSSH.php");
include("Instances.php");
include("Command.php");

use App\ConnectionSSH\ConnectionSSH as ConnectionSSH;
use App\Instances\Instances;
use App\Command\Command;
use Exception;

class Deploy {

    private $connection;
    const SCRIPTS = array(
        'ap'            => 'sudo sh /home/update_ap.sh', 
        'api'           => 'sudo sh /home/update_api.sh',
        'exp'           => 'sudo sh /home/update_2im_exp.sh',
        'gateway'       => 'sudo sh /home/update_gateway.sh',
        'estrutura'     => 'sudo sh /home/update_estrutura.sh',
        '2im_analytics' => 'sudo sh /home/update_2im_analytics.sh',
        '2im_painel'    => 'sudo sh /home/update_2im_painel.sh'        
    );

    function __construct()
    {
        $this->connection = new ConnectionSSH();
    }

    function defineCommand(): string
    {
        $project = getopt(null, ['projeto:']);

        $command = array_filter(self::SCRIPTS, function($key) use ($project) {
            if($key == $project['projeto']) return self::SCRIPTS[$key];            
        }, ARRAY_FILTER_USE_KEY);

        return implode($command);
        
    }

    function getInstances(): array
    {
        $instanceClass = new Instances();
        return $instanceClass->getInstances();
    }
    
    function deployProcess()
    {
        try {
            $command = $this->defineCommand();
            $instances = $this->getInstances();

            if(empty($command)) {
                throw new Exception("Nenhum Comando de Deploy foi encontrado para o projeto", 1);            
            }

            foreach ($instances as $instance) {
                echo "#############################################  Iniciando processo de Deploy para Inst�ncia " . $instance["InstanceId"] . " ############################################" ."\n";
                
                $connect = $this->connection->connect($instance['PublicDnsName']);                
                Command::runCommand($connect, $command);
                // Command::runCommand($connect, "cd /var/www");
                // Command::runCommand($connect, "ls");
                            
                echo "############################################ Processo de Deploy encerrado para Int�ncia " . $instance['InstanceId'] . "############################################" . "\n";
            }

        } catch(Exception $ex) {            
            print_r($ex->getMessage());
            exit(1);
        }
    }    
}

$deploy = new Deploy();
$command = $deploy->deployProcess();