<?php

namespace App\Instances;

use Exception;

class Instances 
{
    const LOAD_BALANCER_ARN = 'arn:aws:elasticloadbalancing:sa-east-1:222648309200:targetgroup/p4p-api/17ebebae841d0a3e';
    const OUTPUT = 'json';

    private function getInstanceIdOfEachInstance(): array 
    {
        $command = "aws elbv2 describe-target-health" 
                . " --target-group-arn " . self::LOAD_BALANCER_ARN 
                . " --query 'TargetHealthDescriptions[*].Target.Id' --output " . self::OUTPUT;

        $idOfEachInstance = shell_exec($command);
        
        if (is_null($idOfEachInstance)) {
            throw new Exception("Nenhuma Instância foi encontrada");
        }

        return json_decode($idOfEachInstance, true);            
    }
        
    public function verifyInstances(string $instanceId): ?array
    {
        $command = 'aws ec2 describe-instances --instance-ids ' . $instanceId;
        $result = shell_exec($command);
        
        if(is_null($result)) {
            throw new Exception("Não foi possível encontrar detalhes da instância EC2, com id: " . $instanceId);
        }
        
        $result = $this->formatToArray($result);                
        $instance = $this->filterArray($result);
                
        return $instance;        
    }
    
    private function formatToArray($result): array
    {
        $result = json_decode($result, true);
        return $result['Reservations'][0]['Instances'][0];
    }

    private function removeStoppedInstaces(array $instances): array
    {
        $instances = array_filter($instances, function($index) {
            if($index['State']['Name'] == 'running') return $index;
        });

        return $instances;
    }

    public function filterArray(array $instance): array
    {
        $allowed = array("InstanceId", "VpcId", "PublicDnsName", "PublicIpAddress", "State");

        $instancefiltered = array_filter($instance, function ($key) use ($allowed) {
            return in_array($key, $allowed);
        }, ARRAY_FILTER_USE_KEY);
        
        return $instancefiltered;
    }

    public function getInstances(): ?array 
    {        
        $idOfEachInstance = $this->getInstanceIdOfEachInstance();                        
        $instances = array_map(array($this, "verifyInstances"), $idOfEachInstance);       
        
        $runningInstances = $this->removeStoppedInstaces($instances);
        
        if(empty($runningInstances)) {            
            throw new Exception("Nenhuma Instância ativa foi encontrada");
        }

        return $runningInstances;        
    }
}
