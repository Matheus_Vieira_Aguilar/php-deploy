<?php
namespace App\ConnectionSSH;

require 'vendor/autoload.php';
use phpseclib\Crypt\RSA;
use phpseclib\Net\SSH2;
use Exception;

class ConnectionSSH 
{
    const ID_RSA = '/var/www/servidor_aws.ppk';    
    const USERNAME = 'ec2-user';
    
    function connect(string $host): SSH2
    {
        $ssh = new SSH2($host);        
        $privateKey = $this->getKey();
        $ssh->login(self::USERNAME, $privateKey);        
        if(!$ssh->isAuthenticated()) {
            throw new Exception("Não foi posível se autenticar no servidor", 1);
        }
        
        return $ssh;
    } 

    private function getKey(): RSA
    {
        $rsa = new RSA();
        $rsa->loadKey(file_get_contents(self::ID_RSA));
        
        return $rsa;
    }
}
