<?php

namespace App\Command;

require 'vendor/autoload.php';
use phpseclib\Net\SSH2;
use Exception;
use RuntimeException;

/**
 * @author Matheus Vieira
 * 
 * Essa Classe é responsável pelo processo de deploy para uma aplicação php dentro de 
 * uma instância EC2.
 */
class Command {

    static function runCommand(SSH2 $ssh, string $command): bool 
    {
        try {            
            $ssh->exec("sudo su");        
            $stream =  $ssh->exec($command);
            $stream =  $ssh->exec("history");
            
            $status = $ssh->getExitStatus();
            
            print_r($stream);
            die();
            if(0 != $status) {
                throw new Exception("Erro ao executar processo de deploy", 1);                
            }
            
            return true;

        } catch (Exception $ex) {
            print($ex->getMessage());                        
            exit(1);
        }
    }
}